################
## ROBOT MSGS ##
################

project(robot_msgs)

cmake_minimum_required(VERSION 2.8.3)

##############
## Packages ##
##############

find_package(catkin REQUIRED
   std_msgs
   message_generation
)

#######################
## Generate messages ##
#######################

## Generate messages in the 'msg' folder
add_message_files(
   FILES
   sensors.msg
   lights.msg
   state.msg
   encoders.msg
   vitals.msg
)

## Generate added messages and services with any dependencies listed here
generate_messages(
   DEPENDENCIES
   std_msgs
)

###################################
## catkin specific configuration ##
###################################

catkin_package(
   CATKIN_DEPENDS message_runtime
)
